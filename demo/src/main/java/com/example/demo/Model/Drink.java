package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "demo_drink")
public class Drink {

    @Id
    @GeneratedValue (strategy =  GenerationType.AUTO)
    private long id ;

    @Column(name = "ten_san_pham")
    private String tenSanPham ; 

    @Column(name = "ma_san_pham")
    private String maSanPham ;

    @Column(name = "price")
    private long price ; 

    @Column(name = "ngay_tao")
    private long ngayTao ; 

    @Column(name = "ngay_cap_nhat")
    private long ngayCapNhat ;

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public long getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public Drink() {
    }

    public Drink(long id, String tenSanPham, String maSanPham, long price, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.tenSanPham = tenSanPham;
        this.maSanPham = maSanPham;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
  
    
}
